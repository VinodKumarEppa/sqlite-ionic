import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AddPage } from '../add/add';
import { EditPage } from '../edit/edit';
import { PassvaluesPage } from '../passvalues/passvalues'; 
import { AlertController } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
export interface Values{
  pname:any;
  pno:any;
  email:any;
  age:any;
  date2:any;
  progress:any;
  hours:any;
  rowid:any;
}

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
expenses:Values[]=[
  {
    pname:"",
    pno:"",
    email:"",
    age:"",
    date2:"",
    progress:"",
    hours:"",
    rowid:""
  }
];
isBoolean=false;
data={rowid:0, progress:"", date2:""};
hour:number;
showNotification;
getNoti;
  constructor(public navCtrl: NavController, private sqlite: SQLite, public navParams: NavParams,
  public alertCtrl: AlertController, private localNotifications: LocalNotifications ) {
  
    this.getNoti=this.navParams.get("hourstohome");
    // this.expenses;
    this.showLocalNotification();    
    this.taskExpiredCondition();
  }

  ionViewDidLoad() {

    this.getData();    
    this.showLocalNotification(); 
    this.taskExpiredCondition();
  }

  ionViewWillEnter() {
    this.getData();
  }

//Shows the Local Notification on screen
showLocalNotification(){
      setInterval(()=> { 
      this.localNotifications.schedule({
      id: 1,
      title: 'App Notification',
      text: 'Check Your App for more details',
      trigger: {at:new Date(new Date().getTime() + 2000)},
      sound:'./assets/mp3/goes-without-saying.mp3',
      data: { "id":1, "name":"Your Notification" }
     });
      this.localNotifications.on('click').subscribe((notification) =>{
      console.log(notification);
     });
     }, 3600000) 
}

//Creates a DataBase and Table and Getting the data from user
  getData() {
  this.sqlite.create({
    name: 'ionicdb.db',
    location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS expense(rowid INTEGER PRIMARY KEY, pname TEXT, email TEXT, pno VARCHAR(10), age INT, date2 TEXT, progress TEXT, hours VARCHAR(5))', [])
        .then(res => console.log('Executed SQL'))
          .catch(e => console.log(e));
            db.executeSql('SELECT * FROM expense ORDER BY rowid DESC', [])
          .then(res => {
          this.expenses=[];
        for(var i=0; i<res.rows.length; i++) {
      this.expenses.push({rowid:res.rows.item(i).rowid,pname:res.rows.item(i).pname,email:res.rows.item(i).email,pno:res.rows.item(i).pno,age:res.rows.item(i).age,date2:res.rows.item(i).date2,progress:res.rows.item(i).progress,hours:res.rows.item(i).hours})
    }
    if(res.rows.length >= 0) {
      this.data.rowid = res.rows.item(0).rowid;
      this.data.progress=res.rows.item(0).progress;
      this.data.date2=res.rows.item(0).date2;
      this.hour=res.rows.item(0).hours;   
    }
  })
    .catch(e => console.log(e));

  }).catch(e => console.log(e));
}

ngOnInit(){
  this.taskExpiredCondition();
}

//for checking hours 
taskExpiredCondition(){ 
  if(this.hour<=0 || this.getNoti<=0){
    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('UPDATE expense SET progress=? WHERE rowid=?',[this.data.progress,this.data.rowid])
        .then(res => {
          this.taskExpired();
          alert("Task updated"+ JSON.stringify(res));
          // this.navCtrl.popToRoot();
  
        }).catch( err => {
          alert(JSON.stringify(err));
        });
      }).catch((err)=>{
        alert(JSON.stringify(err));
      });  
      this.taskExpired();
  }
}

//task expired
taskExpired(){
  this.data.progress="Your Task Expired";
}

//to complete task
taskCompletes(){
  const confirm = this.alertCtrl.create({
    title: 'Complete Task Here',
    message: 'Are You sure you want to complete the task?',
    buttons: [
      {
        text: 'Cancel',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'Confirm',
        handler: () => {
          this.sqlite.create({
            name: 'ionicdb.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('UPDATE expense SET progress=? WHERE rowid=?',[this.data.progress,this.data.rowid])
              .then(res => {
                this.taskUpdate();
                alert("Task updated"+ JSON.stringify(res));
                // this.navCtrl.popToRoot();
        
              }).catch( err => {
                alert(JSON.stringify(err));
              });
            }).catch((err)=>{
              alert(JSON.stringify(err));
            });  
            this.taskUpdate();
            alert("your Hours = "+this.hour);
          console.log('Agree clicked');
        }
      }
    ]
  });
  confirm.present();

}
// for updating task progress to complete
taskUpdate(){
    this.data.progress="Your Task Completed";
    // if(this.data.progress=="Your Task Completed"){
    // this.isBoolean=true;
    // }
}

//Adding user Values
addData() {
  this.navCtrl.push(AddPage);
}

//Edit the User Values
editValues(rowid) {
  this.navCtrl.push(EditPage, {
    "rowids":rowid
  });
}

//sending values to passvalues page
sendValues(rowid){
  this.navCtrl.push(PassvaluesPage, {
    "rowids":rowid
  });
}

//Deleting Data from DataBase
deleteData(rowid) {
  this.sqlite.create({
    name: 'ionicdb.db',
    location: 'default'
  }).then((db: SQLiteObject) => {
    db.executeSql('DELETE FROM expense WHERE rowid=?', [rowid])
    .then(res => {
      console.log(res);
      this.getData();
    })
    .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

//confirms before deleting data
  showConfirm(rowid) {
     const confirm = this.alertCtrl.create({
       title: 'Delete Data Here',
       message: 'Are You sure you want to delete Data?',
       buttons: [
         {
           text: 'Cancel',
           handler: () => {
             console.log('Disagree clicked');
           }
         },
         {
           text: 'Confirm',
           handler: () => {
             this.deleteData(rowid);
             console.log('Agree clicked');
           }
         }
       ]
     });
     confirm.present();
   }
 
}
