import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {
data={pname:"", email:"", pno:"", age:"",  date2:"", progress:""};
hours:number;
todaysDate=new Date();
datetill;
validations_form:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  private sqlite: SQLite, private toast: Toast, private formBuilder: FormBuilder) {
    this.formValidations();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPage');
  }

  ngOnInit(){
    var td=new Date();
    td.setDate(td.getDate()); //retrieving today's date
    this.datetill=td.toISOString().substr(0,10);
  }

//Inserts Users Input values to database table
  saveData() {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('INSERT INTO expense VALUES(NULL,?,?,?,?,?,?,?)',[this.data.pname,this.data.email,this.data.pno,this.data.age,this.data.date2,this.data.progress,this.hours])
          .then(res => {
            console.log(res);
            this.toast.show('Data saved', '5000', 'center').subscribe(
              toast => {
                this.calculateDates();
              
                this.navCtrl.popToRoot();
              }
            );
          })
          .catch(e => {
            console.log(e);
            this.toast.show(e, '5000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          });
      }).catch(e => {
        console.log(e);
        this.toast.show(e, '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
      console.log(this.data);
      this.calculateDates();
      // this.sendHours();
    }

  //Calculating Dates 
    calculateDates(){
      var sec=3600000;
       var one_day=1000*60*60*24;
       var d1 =new Date().getTime();
       var d2 = new Date(this.data.date2).getTime();
       var difference_ms = d2 - d1;
       var remainingDays = Math.round(difference_ms/one_day);
       this.hours=Math.round(difference_ms/sec);
       console.log(remainingDays);
       console.log(this.hours);
      // if(new Date().getTime() <= new Date(this.data.date2).getTime()){
            
      if(this.hours >0){
        this.data.progress="Your Task is in progress";
        console.log("date2 is bigger");
      }
    
  // }
  else if(this.hours <= 0){
    this.data.progress="Your task expired already";
  } 
 }
    // sendHours(){
    //   this.navCtrl.push(HomePage, {
    //     "hours":this.hours
    //   });
    // }

//Forms validations using reactive Approach
    formValidations(){
    this.validations_form=this.formBuilder.group({
      pname: new FormControl('', Validators.compose([
        Validators.pattern('^([a-zA-Z][a-zA-Z ]*[a-zA-Z])+$'),
        Validators.minLength(5),
        Validators.maxLength(20),
        Validators.required
      ])),

      email: new FormControl('', Validators.compose([
        Validators.pattern('^[a-zA-Z]+[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'),
        Validators.required
      ])),

      pno: new FormControl('', Validators.compose([
        Validators.pattern('^[0-9]+$'),
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(10)
      ])),

      age: new FormControl('', Validators.compose([
        Validators.pattern('^[0-9]+$'),
        Validators.minLength(1),
        Validators.maxLength(3),
        Validators.required,
      ])),

      // date:['', Validators.required],
      date2:['', Validators.required]
    });
  }

//Erros Messages of Users input
  validation_messages = {

    'pname': [
      { type: 'required', message: 'Username must be entered.' },

      { type: 'pattern', message: "Username shouln't contain any number or Remove Space In the beginning."},
      {type: 'minlength', message: "Username should be more than 5 letters."},
      {type: 'maxlength', message: "Username should not be more than 20 letters."},


    ],

    'email': [
      { type: 'required', message: 'Email must be entered.'},
      { type: 'pattern', message: 'Please enter valid email.'},
    ],

    'pno': [
      { type: 'required', message: 'Mobile Number is required' },
      { type: 'pattern', message: 'Please enter a valid mobile number.' },
      { type: 'minlength', message: 'Invalid Number.' },
      { type: 'maxlength', message: 'Invalid Number' },
    ],

    'age': [
      {type: 'required', message: 'Age must be entered'},
      {type: 'pattern', message: 'Please enter valid age'},
      {type: 'minlength', message: 'invalid age'},
      {type: 'maxlength', message: 'invalid age'},
    ],

    // 'date': [
    //   { type: 'required', message: 'Date and Time is required'},
    // ],

    'date2': [
      { type: 'required', message: 'Date and Time is required'},
    ]

  }

}
