import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PassvaluesPage } from './passvalues';

@NgModule({
  declarations: [
    PassvaluesPage,
  ],
  exports:[
    PassvaluesPage,
  ],
  imports: [
    IonicPageModule.forChild(PassvaluesPage),
  ],
})
export class PassvaluesPageModule {}
