import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-passvalues',
  templateUrl: 'passvalues.html',
})
export class PassvaluesPage {
  data={rowid:0, pname:"", email:"", pno:"", age:"",  date2:"", progress:"",};
  hours:number;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private sqlite: SQLite, private toast: Toast) {
      this.getCurrentData(navParams.get("rowids"));
  }

  //Function which Gets Data From user and Same data Displays on Screen so we can edit
  getCurrentData(rowid) {
    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM expense WHERE rowid=?', [rowid])
        .then(res => {
          if(res.rows.length >= 0) {
            this.data.rowid = res.rows.item(0).rowid;
            this.data.pname = res.rows.item(0).pname;
            this.data.email = res.rows.item(0).email;
            this.data.pno = res.rows.item(0).pno;
            this.data.age = res.rows.item(0).age;
            // this.data.date = res.rows.item(0).date;
            this.data.date2 = res.rows.item(0).date2;
            this.data.progress=res.rows.item(0).progress;
            this.hours=res.rows.item(0).hours;
            
          }
        })
        .catch(e => {
          console.log(e);
          this.toast.show(e, '5000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    }).catch(e => {
      console.log(e);
      this.toast.show(e, '5000', 'center').subscribe(
        toast => {
          console.log(toast);
        }
      );
    });   
  }

  //for update color based on the condition
  getColor(progress){
    if(this.data.progress=="Your Task is in progress"){
      return 'green';
    }
    else if(this.data.progress=="Your Task Completed"){
      return 'orange';
    }
    else if(this.data.progress=="Your Task Expired"){
      return 'red';
    }
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad PassvaluesPage');
  }

}
